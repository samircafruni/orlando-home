<!doctype html>
<html lang="en">
<head>

<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<meta name="theme-color" content="#001234">
<meta name="apple-mobile-web-app-status-bar-style" content="#001234">
<meta name="msapplication-navbutton-color" content="#001234">

<!-- CSS -->
<link type="text/css" href="css/index.css" rel="stylesheet"/>
<link type="text/css" href="css/tiny-slider.css" rel="stylesheet"/>

</head>
<body>

    <!-- Facebook Sharing -->
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v11.0&appId=2976456275907063&autoLogAppEvents=1" nonce="f0NeT5kq"></script>

    <!-- Top Header -->
    <section id="top">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex align-items-center justify-content-between">

                    <!-- Logo Mobile -->
                    <img class="d-block d-lg-none logo" src="./img/logo-orlando-color.svg" alt="Logo">

                    <!-- Nav -->
                    <nav class="main-nav main-nav-hide">
                        <ul class="main-nav-items">
                            <li><a href="#" class="main-nav-link">Home</a></li>
                            <li><a href="#" class="main-nav-link">Find a Home</a></li>
                            <li><a href="#" class="main-nav-link">Areas</a></li>
                            <li><a href="#" class="main-nav-link">Sell a Home</a></li>
                            <li><a href="#" class="main-nav-link">Resources</a></li>
                            <li><a href="#" class="main-nav-link">Blog</a></li>
                            <li><a href="#" class="main-nav-link">About Us</a></li>
                        </ul>
                    </nav>

                    <!-- Languages -->
                    <ul class="nav-lang d-none d-lg-flex align-items-center">
                        <li><a href="#"><img src="./img/lang-en.svg" alt="English"></a>
                        </li>
                        <li><a href="#"><img src="./img/lang-pt.svg" alt="Portuguese"></a>
                        </li>
                        <li><a href="#"><img src="./img/lang-es.svg" alt="Spanish"></a>
                        </li>
                    </ul>

                    <button class="main-nav-action d-lg-none">
                        <img src="./img/icon-menu.png" alt="Button">
                    </button>
                </div>
            </div>
        </div>
    </section>

    <!-- Header -->
    <section id="header">
        <div class="container">
            <div class="row align-items-center">

                <!-- Col-1 Infos -->
                <div class="col-lg-4 d-none d-lg-flex align-items-center justify-content-between">
                    <div class="container-phone d-flex align-items-center">
                        <img class="icon mr-15" src="./img/icon-message-gradient.svg" alt="Call Us">
                        <a href="tel:14074401010" class="font-family-montserrat font-color-color-1">+1 (407) <span class="font-weight-bold">44.1010</span></a>
                    </div>
                    <div class="container-social">
                        <ul class="d-flex align-items-center">
                            <li>
                                <a href="https://www.facebook.com/casasavendaorlando/" target="_blank">
                                    <img class="icon mr-15" src="./img/icon-facebook-square.svg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/authenticorlando/" target="_blank">
                                    <img class="icon mr-15" src="./img/icon-instagram.svg" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/c/authenticrealestate" target="_blank">
                                    <img class="icon mr-15" src="./img/icon-youtube-square.svg" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- Col-2 Logo -->
                <div class="col-12 col-md-6 offset-md-3 offset-lg-0 col-lg-4 flex-column align-items-center justify-content-between">

                    <!-- Logo -->
                    <img class="logo d-none d-md-block mb-15" src="./img/logo-orlando-color.svg" alt="Logo">

                    <!-- CTAS -->
                    <div class="container-ctas d-flex align-items-center justify-content-center">
                        <div class="container-chat d-flex d-lg-none align-items-center mr-20">
                            <img class="icon mr-15" src="./img/icon-user-tie.svg" alt="Talk to an Agent">
                            <div class="d-flex flex-column justify-content-center font-family-montserrat text-uppercase">
                                <span class="font-size-11">Talk to an</span>
                                <span class="font-size-18 font-weight-bold">Agent</span>
                            </div>
                        </div>

                        <div class="container-form d-flex d-lg-none align-items-center">
                            <img class="icon mr-15" src="./img/icon-phone-alt.svg" alt="Request a Phone Call">
                            <div class="d-flex flex-column justify-content-center font-family-montserrat font-size-11 font-color-color-1 text-uppercase">
                                <span class="font-size-11">Request a</span>
                                <span class="font-size-18 font-weight-bold">Phone Call</span>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- Col-3 Infos -->
                <div class="col-12 col-lg-4 d-none d-lg-flex align-items-center justify-content-between">
                    <div class="container-chat d-flex align-items-center">
                        <img class="icon mr-15" src="./img/icon-user-tie.svg" alt="Talk to an Agent">
                        <div class="d-flex flex-column justify-content-center font-family-montserrat text-uppercase">
                            <span class="font-size-11">Talk to an</span>
                            <span class="font-size-18 font-weight-bold">Agent</span>
                        </div>
                    </div>
                    <div class="container-form d-flex align-items-center">
                        <img class="icon mr-15" src="./img/icon-phone-alt.svg" alt="Request a Phone Call">
                        <div class="d-flex flex-column justify-content-center font-family-montserrat font-size-11 font-color-color-1 text-uppercase">
                            <span class="font-size-11">Request a</span>
                            <span class="font-size-18 font-weight-bold">Phone Call</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Breadcrumbs -->
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">

                <!-- Breadcrumbs -->
                <div class="col-7 col-lg-8">
                    <ul class="d-flex align-items-center font-family-montserrat font-size-14 font-color-white text-uppercase"">
                        <li class="breadcrumb-item d-none d-md-inline-flex font-weight-bold">Home</li>
                        <li class="breadcrumb-item ">13837 GOLDEN RUSSET DR, WINTER GARDEN, FL 34787</li>
                    </ul>
                </div>

                <!-- Share -->
                <div class="col-5 col-lg-4 d-flex justify-content-end">

                    <!-- Share Links -->
                    <ul class="share-items d-inline-flex align-items-center font-family-montserrat font-size-14 font-color-white">
                        <li class="share-item d-none d-md-block mr-15">Compartilhe:</li>
                        <li class="share-item">
                            <a target="_blank" href="https://casasavendaorlando.com.br" class="fb-xfbml-parse-ignore">
                                <img src="./img/icon-facebook-f.svg" alt="Facebook share">
                            </a>
                        </li>
                        <li class="share-item">
                            <a href="whatsapp://send?text=https://casasavendaorlando.com.br" target="_blank">
                                <img src="./img/icon-whatsapp.svg" alt="WhatsApp share">
                            </a>
                        </li>
                        <li class="share-item">
                            <a href="https://twitter.com/home?status=https://casasavendaorlando.com.br" target="_blank">
                                <img src="./img/icon-twitter.svg" alt="Twitter share">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Featured -->
    <section id="featured">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex flex-column justify-content-center align-items-center">
                    <h5 class="font-family-montserrat font-size-16 font-color-color-1 text-uppercase">Homes for sale in</h5>
                    <h1 class="font-family-montserrat font-size-44 font-color-color-1 font-weight-extra-bold text-uppercase">CHAMPIONS GATE RESORT</h1>
                    <div class="separator"></div>
                    <ul class="d-inline-flex align-items-center">
                        <li class="font-family-montserrat font-size-16 font-color-color-1 text-uppercase mr-15">starting at</li>
                        <li class="font-family-montserrat font-size-32 font-color-color-1 font-weight-bold text-uppercase mr-15">U$ 481,990</li>
                        <li class="font-family-montserrat font-size-16 font-color-color-1 icon-inline icon-bed mr-15 d-none d-lg-flex">2 to 8 bed</li>
                        <li class="font-family-montserrat font-size-16 font-color-color-1 icon-inline icon-bath mr-15 d-none d-lg-flex">2 to 8 bath</li>
                        <li class="font-family-montserrat font-size-16 font-color-color-1 icon-inline icon-area mr-15 d-none d-lg-flex">1905 - 3810 SqFt</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Form Info -->
    <section id="form-info">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <img class="w-100" src="./img/img-1.png" alt="Image">
                </div>
                <div class="col-lg-6">

                    <h3 class="font-family-montserrat font-size-21 font-color-color-1 text-uppercase">request more</h3>
                    <h1 class="font-family-montserrat font-size-58 font-color-color-1 font-weight-bold text-uppercase">INFORMATION</h1>
                    <div class="separator mb-20"></div>

                    <!-- Form -->
                    <form action="#">
                        <div class="form-row mb-15">
                            <div class="col-12 col-lg">
                                <input type="text" class="form-control" placeholder="First name">
                            </div>
                            <div class="col-12 col-lg">
                                <input type="text" class="form-control" placeholder="Last name">
                            </div>
                            <div class="col-12">
                                <input type="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="col-12 col-lg">
                                <select name="" class="form-control">
                                    <option value="" disabled selected>Select a country code</option>
                                    <option value="">Brazil</option>
                                    <option value="">USA</option>
                                    <option value="">Canada</option>
                                    <option value="">...</option>
                                </select>
                            </div>
                            <div class="col-12 col-lg">
                                <input type="text" class="form-control" placeholder="Phone">
                            </div>
                        </div>
                        <div class="form-row mb-15">
                            <div class="col-12 d-flex flex-column justify-content-start">
                                <span class="font-family-montserrat font-size-13 font-color-color-9 font-weight-bold text-uppercase mb-10">CONTACT PREERENCE:</span>
                                <div class="check-container d-flex align-items-center">
                                    <input type="checkbox" id="pref_text" value="">
                                    <label for="pref_text">
                                        <span class="font-family-montserrat font-size-13 font-color-color-9">Text</span>
                                    </label>
                                    <input type="checkbox" id="pref_text" value="">
                                    <label for="pref_call">
                                        <span class="font-family-montserrat font-size-13 font-color-color-9">Call</span>
                                    </label>
                                    <input type="checkbox" id="pref_text" value="">
                                    <label for="pref_mail">
                                        <span class="font-family-montserrat font-size-13 font-color-color-9">Email</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row mb-15">
                            <div class="container">
                                <div class="col-12">
                                    <textarea class="form-control w-100" rows="6" placeholder="Message"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col d-flex justify-content-end">
                                <button class="btn btn-gradient icon-arrow-right btn-lg">
                                    <span class="mr-5">Send</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- SubNav -->
    <section id="nav">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="secondary-nav">
                        <ul class="secondary-nav-items justify-content-lg-around font-family-montserrat font-color-white font-size-13 text-uppercase">
                            <li class="secondary-nav-item active">
                                <a href="#houses-for-sale" class="icon-house">Houses for sale</a>
                            </li>
                            <li class="secondary-nav-item">
                                <a href="#income-projection" class="icon-income-project">Income projection</a>
                            </li>
                            <li class="secondary-nav-item">
                                <a href="#financing" class="icon-financing-mono">Financing</a>
                            </li>
                            <li class="secondary-nav-item">
                                <a href="#floor-plan" class="icon-floor-plan">Floor Plan</a>
                            </li>
                            <li class="secondary-nav-item">
                                <a href="#overview" class="icon-overview">Overview</a>
                            </li>
                            <li class="secondary-nav-item">
                                <a href="#contact-us" class="icon-contact-us">Contact us</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>

    <!-- Houses for Sale -->
    <section id="houses-for-sale">
        <div class="container">

            <!-- Section Title -->
            <div class="row">
                <div class="col d-flex flex-column align-items-center justify-content-center">
                    <h3 class="font-family-montserrat font-size-22 font-color-color-1 text-uppercase">Available</h3>
                    <h1 class="font-family-montserrat font-size-42 font-color-color-1 text-uppercase font-weight-bold">Homes</h1>
                    <div class="separator mb-60"></div>
                </div>
            </div>

            <!-- Property Cards -->
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4 mt-15 mb-15 pl-15 pr-15">
                    <div class="card">

                        <!-- Featured Image -->
                        <img class="featured-img w-100" src="./img/img-61.png" alt="Property">

                        <!-- Address -->
                        <div class="address-container d-flex align-items-center justify-content-between mt-35">
                            <div class="icon-map bg-icon"></div>
                            <span class="font-family-montserrat font-size-14 font-color-color-1 font-weight-bold text-uppercase text-left">
                                13837 GOLDEN RUSSET DR WINTER GARDEN, FL 34787
                            </span>
                        </div>

                        <!-- Price -->
                        <div class="price-container d-flex align-items-center justify-content-around mt-35">
                            <div class="price d-flex align-items-center">
                                <div class="icon-financing-color bg-icon mr-15"></div>
                                <span class="price-text font-family-montserrat font-color-color-1 font-size-24 font-weight-bold text-uppercase">$ 5.300,00</span>
                            </div>
                            <div class="badge badge-active">Active</div>
                        </div>

                        <!-- List Infos -->
                        <ul class="info-items d-flex align-items-center justify-content-between font-family-montserrat font-size-13 font-color-color-1 mt-15">
                            <li class="d-flex align-items-center"><div class="icon-bed bg-icon mr-5"></div>9 beds</li>
                            <li class="d-flex align-items-center"><div class="icon-bath bg-icon mr-5"></div>5 Baths</li>
                            <li class="d-flex align-items-center"><div class="icon-area bg-icon mr-5"></div>3,254 SqFt</li>
                        </ul>

                        <!-- SKU -->
                        <ul class="sku-container bg-color-4 d-flex align-items-center justify-content-center font-family-montserrat font-color-color-9 font-size-12 mt-15 pt-5 pb-5">
                            <li class="text-uppercase">MLS #: S5036731</li>
                            <li class="ml-15 mr-15">|</li>
                            <li>Single Family</li>
                        </ul>

                        <!-- CTA -->
                        <ul class="cta-container d-flex align-items-center justify-content-between font-family-montserrat font-weight-bold text-uppercase mt-25">
                            <li>
                                <a href="#" class="btn icon-play-circle font-size-14 font-color-color-1" style="text-decoration: underline;">Virtual Tour</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-dark-blue font-color-white font-size-12">See Property</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 mt-15 mb-15 pl-15 pr-15">
                    <div class="card">

                        <!-- Featured Image -->
                        <img class="featured-img w-100" src="./img/img-61.png" alt="Property">
                        <div class="badge badge-increase">Price increase</div>

                        <!-- Address -->
                        <div class="address-container d-flex align-items-center justify-content-between mt-35">
                            <div class="icon-map bg-icon"></div>
                            <span class="font-family-montserrat font-size-14 font-color-color-1 font-weight-bold text-uppercase text-left">
                                13837 GOLDEN RUSSET DR WINTER GARDEN, FL 34787
                            </span>
                        </div>

                        <!-- Price -->
                        <div class="price-container d-flex align-items-center justify-content-around mt-35">
                            <div class="price d-flex align-items-center">
                                <div class="icon-financing-color bg-icon mr-15"></div>
                                <span class="price-text font-family-montserrat font-color-color-1 font-size-24 font-weight-bold text-uppercase">$ 5.300,00</span>
                            </div>
                            <div class="badge badge-active">Active</div>
                        </div>

                        <!-- List Infos -->
                        <ul class="info-items d-flex align-items-center justify-content-between font-family-montserrat font-size-13 font-color-color-1 mt-15">
                            <li class="d-flex align-items-center"><div class="icon-bed bg-icon mr-5"></div>9 beds</li>
                            <li class="d-flex align-items-center"><div class="icon-bath bg-icon mr-5"></div>5 Baths</li>
                            <li class="d-flex align-items-center"><div class="icon-area bg-icon mr-5"></div>3,254 SqFt</li>
                        </ul>

                        <!-- SKU -->
                        <ul class="sku-container bg-color-4 d-flex align-items-center justify-content-center font-family-montserrat font-color-color-9 font-size-12 mt-15 pt-5 pb-5">
                            <li class="text-uppercase">MLS #: S5036731</li>
                            <li class="ml-15 mr-15">|</li>
                            <li>Single Family</li>
                        </ul>

                        <!-- CTA -->
                        <ul class="cta-container d-flex align-items-center justify-content-between font-family-montserrat font-weight-bold text-uppercase mt-25">
                            <li>
                                <a href="#" class="btn icon-play-circle font-size-14 font-color-color-1" style="text-decoration: underline;">Virtual Tour</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-dark-blue font-color-white font-size-12">See Property</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 mt-15 mb-15 pl-15 pr-15">
                    <div class="card">

                        <!-- Featured Image -->
                        <img class="featured-img w-100" src="./img/img-69.png" alt="Property">

                        <!-- Address -->
                        <div class="address-container d-flex align-items-center justify-content-between mt-35">
                            <div class="icon-map bg-icon"></div>
                            <span class="font-family-montserrat font-size-14 font-color-color-1 font-weight-bold text-uppercase text-left">
                                13837 GOLDEN RUSSET DR WINTER GARDEN, FL 34787
                            </span>
                        </div>

                        <!-- Price -->
                        <div class="price-container d-flex align-items-center justify-content-around mt-35">
                            <div class="price d-flex align-items-center">
                                <div class="icon-financing-color bg-icon mr-15"></div>
                                <span class="price-text font-family-montserrat font-color-color-1 font-size-24 font-weight-bold text-uppercase">$ 5.300,00</span>
                            </div>
                            <div class="badge badge-active">Active</div>
                        </div>

                        <!-- List Infos -->
                        <ul class="info-items d-flex align-items-center justify-content-between font-family-montserrat font-size-13 font-color-color-1 mt-15">
                            <li class="d-flex align-items-center"><div class="icon-bed bg-icon mr-5"></div>9 beds</li>
                            <li class="d-flex align-items-center"><div class="icon-bath bg-icon mr-5"></div>5 Baths</li>
                            <li class="d-flex align-items-center"><div class="icon-area bg-icon mr-5"></div>3,254 SqFt</li>
                        </ul>

                        <!-- SKU -->
                        <ul class="sku-container bg-color-4 d-flex align-items-center justify-content-center font-family-montserrat font-color-color-9 font-size-12 mt-15 pt-5 pb-5">
                            <li class="text-uppercase">MLS #: S5036731</li>
                            <li class="ml-15 mr-15">|</li>
                            <li>Single Family</li>
                        </ul>

                        <!-- CTA -->
                        <ul class="cta-container d-flex align-items-center justify-content-between font-family-montserrat font-weight-bold text-uppercase mt-25">
                            <li>
                                <a href="#" class="btn icon-play-circle font-size-14 font-color-color-1" style="text-decoration: underline;">Virtual Tour</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-dark-blue font-color-white font-size-12">See Property</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 mt-15 mb-15 pl-15 pr-15">
                    <div class="card">

                        <!-- Featured Image -->
                        <img class="featured-img w-100" src="./img/img-11.png" alt="Property">
                        <div class="badge badge-new">New Listing</div>

                        <!-- Address -->
                        <div class="address-container d-flex align-items-center justify-content-between mt-35">
                            <div class="icon-map bg-icon"></div>
                            <span class="font-family-montserrat font-size-14 font-color-color-1 font-weight-bold text-uppercase text-left">
                                13837 GOLDEN RUSSET DR WINTER GARDEN, FL 34787
                            </span>
                        </div>

                        <!-- Price -->
                        <div class="price-container d-flex align-items-center justify-content-around mt-35">
                            <div class="price d-flex align-items-center">
                                <div class="icon-financing-color bg-icon mr-15"></div>
                                <span class="price-text font-family-montserrat font-color-color-1 font-size-24 font-weight-bold text-uppercase">$ 5.300,00</span>
                            </div>
                            <div class="badge badge-active">Active</div>
                        </div>

                        <!-- List Infos -->
                        <ul class="info-items d-flex align-items-center justify-content-between font-family-montserrat font-size-13 font-color-color-1 mt-15">
                            <li class="d-flex align-items-center"><div class="icon-bed bg-icon mr-5"></div>9 beds</li>
                            <li class="d-flex align-items-center"><div class="icon-bath bg-icon mr-5"></div>5 Baths</li>
                            <li class="d-flex align-items-center"><div class="icon-area bg-icon mr-5"></div>3,254 SqFt</li>
                        </ul>

                        <!-- SKU -->
                        <ul class="sku-container bg-color-4 d-flex align-items-center justify-content-center font-family-montserrat font-color-color-9 font-size-12 mt-15 pt-5 pb-5">
                            <li class="text-uppercase">MLS #: S5036731</li>
                            <li class="ml-15 mr-15">|</li>
                            <li>Single Family</li>
                        </ul>

                        <!-- CTA -->
                        <ul class="cta-container d-flex align-items-center justify-content-between font-family-montserrat font-weight-bold text-uppercase mt-25">
                            <li>
                                <a href="#" class="btn icon-play-circle font-size-14 font-color-color-1" style="text-decoration: underline;">Virtual Tour</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-dark-blue font-color-white font-size-12">See Property</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 mt-15 mb-15 pl-15 pr-15">
                    <div class="card">

                        <!-- Featured Image -->
                        <img class="featured-img w-100" src="./img/img-53.png" alt="Property">
                        <div class="badge badge-decrease">Price decrease</div>

                        <!-- Address -->
                        <div class="address-container d-flex align-items-center justify-content-between mt-35">
                            <div class="icon-map bg-icon"></div>
                            <span class="font-family-montserrat font-size-14 font-color-color-1 font-weight-bold text-uppercase text-left">
                                13837 GOLDEN RUSSET DR WINTER GARDEN, FL 34787
                            </span>
                        </div>

                        <!-- Price -->
                        <div class="price-container d-flex align-items-center justify-content-around mt-35">
                            <div class="price d-flex align-items-center">
                                <div class="icon-financing-color bg-icon mr-15"></div>
                                <span class="price-text font-family-montserrat font-color-color-1 font-size-24 font-weight-bold text-uppercase">$ 5.300,00</span>
                            </div>
                            <div class="badge badge-active">Active</div>
                        </div>

                        <!-- List Infos -->
                        <ul class="info-items d-flex align-items-center justify-content-between font-family-montserrat font-size-13 font-color-color-1 mt-15">
                            <li class="d-flex align-items-center"><div class="icon-bed bg-icon mr-5"></div>9 beds</li>
                            <li class="d-flex align-items-center"><div class="icon-bath bg-icon mr-5"></div>5 Baths</li>
                            <li class="d-flex align-items-center"><div class="icon-area bg-icon mr-5"></div>3,254 SqFt</li>
                        </ul>

                        <!-- SKU -->
                        <ul class="sku-container bg-color-4 d-flex align-items-center justify-content-center font-family-montserrat font-color-color-9 font-size-12 mt-15 pt-5 pb-5">
                            <li class="text-uppercase">MLS #: S5036731</li>
                            <li class="ml-15 mr-15">|</li>
                            <li>Single Family</li>
                        </ul>

                        <!-- CTA -->
                        <ul class="cta-container d-flex align-items-center justify-content-between font-family-montserrat font-weight-bold text-uppercase mt-25">
                            <li>
                                <a href="#" class="btn icon-play-circle font-size-14 font-color-color-1" style="text-decoration: underline;">Virtual Tour</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-dark-blue font-color-white font-size-12">See Property</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 mt-15 mb-15 pl-15 pr-15">
                    <div class="card">

                        <!-- Featured Image -->
                        <img class="featured-img w-100" src="./img/img-59.png" alt="Property">

                        <!-- Address -->
                        <div class="address-container d-flex align-items-center justify-content-between mt-35">
                            <div class="icon-map bg-icon"></div>
                            <span class="font-family-montserrat font-size-14 font-color-color-1 font-weight-bold text-uppercase text-left">
                                13837 GOLDEN RUSSET DR WINTER GARDEN, FL 34787
                            </span>
                        </div>

                        <!-- Price -->
                        <div class="price-container d-flex align-items-center justify-content-around mt-35">
                            <div class="price d-flex align-items-center">
                                <div class="icon-financing-color bg-icon mr-15"></div>
                                <span class="price-text font-family-montserrat font-color-color-1 font-size-24 font-weight-bold text-uppercase">$ 5.300,00</span>
                            </div>
                            <div class="badge badge-active">Active</div>
                        </div>

                        <!-- List Infos -->
                        <ul class="info-items d-flex align-items-center justify-content-between font-family-montserrat font-size-13 font-color-color-1 mt-15">
                            <li class="d-flex align-items-center"><div class="icon-bed bg-icon mr-5"></div>9 beds</li>
                            <li class="d-flex align-items-center"><div class="icon-bath bg-icon mr-5"></div>5 Baths</li>
                            <li class="d-flex align-items-center"><div class="icon-area bg-icon mr-5"></div>3,254 SqFt</li>
                        </ul>

                        <!-- SKU -->
                        <ul class="sku-container bg-color-4 d-flex align-items-center justify-content-center font-family-montserrat font-color-color-9 font-size-12 mt-15 pt-5 pb-5">
                            <li class="text-uppercase">MLS #: S5036731</li>
                            <li class="ml-15 mr-15">|</li>
                            <li>Single Family</li>
                        </ul>

                        <!-- CTA -->
                        <ul class="cta-container d-flex align-items-center justify-content-between font-family-montserrat font-weight-bold text-uppercase mt-25">
                            <li>
                                <a href="#" class="btn icon-play-circle font-size-14 font-color-color-1" style="text-decoration: underline;">Virtual Tour</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-dark-blue font-color-white font-size-12">See Property</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- CTA -->
            <div class="row mt-60">
                <div class="col-12 d-flex justify-content-center">
                    <button class="btn btn-lg btn-gradient icon-arrow-right font-size-16">
                        <span class="mr-5">See all properties for sale</span>
                    </button>
                </div>
            </div>
        </div>
    </section>

    <!-- Income Projection -->
    <section id="income-projection">
        <div class="container">

            <!-- Section Title -->
            <div class="row">
                <div class="col d-flex flex-column align-items-center justify-content-center">
                    <h3 class="font-family-montserrat font-size-22 font-color-color-1 text-uppercase">Income</h3>
                    <h1 class="font-family-montserrat font-size-42 font-color-color-1 text-uppercase font-weight-bold">Projection</h1>
                    <div class="separator mb-60"></div>
                </div>
            </div>

            <!-- Card -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">

                                <!-- Community -->
                                <div class="col col-12 col-md-6 col-lg-3 d-flex align-items-center justify-content-center">
                                    <div class="community-container divisor d-flex align-items-center justify-content-center">
                                        <div class="icon-map-marked-gradient mr-10"></div>
                                        <span class="community-text text-container font-family-montserrat font-color-color-1 font-size-16 font-weight-bold text-uppercase">
                                            Champions Gate
                                        </span>
                                    </div>
                                </div>

                                <!-- Property Type -->
                                <div class="col col-12 col-md-6 col-lg-3 d-flex align-items-center justify-content-center">
                                    <div class="property-container divisor d-flex align-items-center justify-content-center">
                                        <div class="icon-home-gradient mr-5"></div>
                                        <span class="property-text text-container font-family-montserrat font-color-color-1 font-size-16 font-weight-bold text-uppercase">
                                            Single Family
                                        </span>
                                    </div>
                                </div>

                                <!-- Beds -->
                                <div class=" col-12 col-md-6 col-lg-3 d-flex align-items-center justify-content-center">
                                    <div class="beds-container divisor d-flex align-items-center justify-content-center">
                                        <div class="icon-bed-gradient mr-5"></div>
                                        <span class="beds-text text-container font-family-montserrat font-color-color-1 font-size-16 font-weight-bold text-uppercase">
                                            9
                                        </span>
                                    </div>
                                </div>

                                <!-- Property Price -->
                                <div class="col col-12 col-md-6 col-lg-3 d-flex align-items-center justify-content-center">
                                    <div class="price-container d-flex align-items-center justify-content-center">
                                        <div class="icon-financing-gradient mr-5"></div>
                                        <span class="price-text text-container font-family-montserrat font-color-color-1 font-size-16 font-weight-bold text-uppercase">
                                            $570,000
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">

                            <!-- CTA -->
                            <div class="row pt-15 pb-15">
                                <div class="col-12 d-flex justify-content-center">
                                    <button class="btn btn-gradient icon-videocam font-size-12">
                                        <span class="mr-5">Watch our explanatory video</span>
                                    </button>
                                </div>
                            </div>

                            <!-- Table -->
                            <div class="row">
                                <div class="col-12">
                                    <div class="table-responsive">

                                        <table class="gross-income no-border-bottom">
                                            <thead>
                                                <tr>
                                                    <th class="main border-bottom" colspan="4">
                                                        <div class="container">
                                                            <span class="font-family-montserrat font-size-18 font-color-color-1 font-weight-bold text-uppercase">Gross Income</span>
                                                            <div class="separator"></div>
                                                        </div>
                                                        <img class="logo" src="./img/logo-table.svg" alt="">
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td rowspan="4" class="font-color-color-1 font-weight-bold" style="width: 222px;">Average Nightly Rate</td>
                                                    <td rowspan="4" class="font-color-color-1 font-weight-bold"  style="width: 222px;">MGMT. Commission</td>
                                                    <td rowspan="4" class="font-color-color-1 font-weight-bold"  style="width: 222px;">Nightly net rate</td>
                                                    <td class="" style="width: 465px; padding-left: 0;">
                                                        <table class="no-border">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="font-weight-bold text-uppercase text-center border-bottom" colspan="3" style="padding: 0">Annual Occupancy Rate</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="font-size-22 font-color-gradient font-weight-bold text-center" style="padding: 0">70%</td>
                                                                    <td class="font-size-22 font-color-gradient font-weight-bold text-center" style="padding: 0">60%</td>
                                                                    <td class="font-size-22 font-color-gradient font-weight-bold text-center" style="padding: 0">80%</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="font-weight-bold border-bottom border-top" colspan="3">Gross Income</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="font-color-color-9 font-weight-bold">$64,824</td>
                                                                    <td class="font-color-color-9 font-weight-bold">$75,628</td>
                                                                    <td class="font-color-color-9 font-weight-bold">$86,432</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="expenses no-border-bottom">
                                            <thead>
                                                <tr>
                                                    <th class="main border-bottom" colspan="6">
                                                        Expenses
                                                        <div class="separator"></div>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th class="border-bottom">Description:</th>
                                                    <th class="border-bottom">Monthly</th>
                                                    <th class="border-bottom">Annually</th>
                                                    <th class="border-bottom text-uppercase text-center" colspan="3" style="padding: 0">Expenses per occupancy rate</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td style="width: 222px">Management</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 222px">Pool Care</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 222px">Pest Control</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 222px">Energy</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 222px">Water</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 222px">Gas</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 222px">Cleaning</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 222px">HOA</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 222px">Property Tax</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 222px">Insurance</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 222px">CDD</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 222px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                    <td style="width: 155px">$99</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3" class="font-color-color-1" style="width: 666px; padding-right: 20px; text-align: right;">Total expenses:</td>
                                                    <td class="font-color-color-1 font-weight-bold" style="width: 155px">$1000,00</td>
                                                    <td class="font-color-color-1 font-weight-bold" style="width: 155px">$1000,00</td>
                                                    <td class="font-color-color-1 font-weight-bold" style="width: 155px">$1000,00</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <table class="net-income">
                                            <thead>
                                                <tr>
                                                    <th class="main" colspan="4">
                                                        Net Income
                                                        <div class="separator"></div>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td style="width: 666px">Net income without mgmt comission</td>
                                                <td class="font-color-color-1 font-weight-bold" style="width: 155px">$10,000</td>
                                                <td class="font-color-color-1 font-weight-bold" style="width: 155px">$20,000</td>
                                                <td class="font-color-color-1 font-weight-bold" style="width: 155px">$30,000</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 666px">Net income with mgmt comission</td>
                                                <td class="font-color-color-1 font-weight-bold" style="width: 155px">$15,000</td>
                                                <td class="font-color-color-1 font-weight-bold" style="width: 155px">$25,000</td>
                                                <td class="font-color-color-1 font-weight-bold" style="width: 155px">$35,000</td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="card-footer">
                            <div class="row">
                                <div class="col-12">

                                    <!-- Table Nav -->
                                    <nav class="table-nav">
                                        <ul class="table-nav-items justify-content-lg-around font-family-montserrat font-size-16 font-color-color-1 font-weight-bold text-uppercase">
                                            <li class="table-nav-item active">2 beds</li>
                                            <li class="table-nav-item">3 beds</li>
                                            <li class="table-nav-item">4 beds</li>
                                            <li class="table-nav-item">5 beds</li>
                                            <li class="table-nav-item">6 beds</li>
                                            <li class="table-nav-item">7 beds</li>
                                            <li class="table-nav-item">8 beds</li>
                                            <li class="table-nav-item">9 beds</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Financing -->
    <section id="financing">
        <div class="container">

            <!-- Section Title -->
            <div class="row">
                <div class="col d-flex flex-column align-items-center justify-content-center">
                    <h3 class="font-family-montserrat font-size-22 font-color-color-1 text-uppercase">Income</h3>
                    <h1 class="font-family-montserrat font-size-42 font-color-color-1 text-uppercase font-weight-bold">Projection</h1>
                    <div class="separator mb-60"></div>
                </div>
            </div>

            <!-- Form -->
            <div class="row">
                <div class="col-lg-8 pl-15 pr-15 mb-30">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-start font-family-montserrat font-size-16 font-color-color-9">
                            <p><span class="font-weight-bold">Fill out the fields</span> to estimate your mortgage payment</p>
                        </div>
                        <div class="card-body">
                            <form action="#">
                                <div class="form-row">
                                    <div class="col-12">
                                        <label class="font-family-montserrat font-size-16 font-color-color-1 font-weight-bold text-uppercase" for="property-price">Property Price</label>
                                        <input id="property-price" type="text" class="form-control mt-10 mb-15" placeholder="$ 300,000.00">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-md-6">
                                        <label class="font-family-montserrat font-size-16 font-color-color-1 font-weight-bold text-uppercase" for="interest-rate">Interest rate</label>
                                        <input id="interest-rate" type="text" class="form-control mt-10 mb-15" placeholder="2,760 %">
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label class="font-family-montserrat font-size-16 font-color-color-1 font-weight-bold text-uppercase" for="downpayment">Downpayment</label>
                                        <input id="downpayment" type="text" class="form-control mt-10 mb-15" placeholder="$ 90,000.00">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 pl-15 pr-15 mb-15">
                    <div class="result-container">
                        <span class="font-size-16">Monthly Payment (P&I)</span>
                        <span class="font-size-50 font-weight-bold get-result">$ 1,499.00</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Income Projection -->
    <section id="floor-plan">
        <div class="container">

            <!-- Section Title -->
            <div class="row">
                <div class="col d-flex flex-column align-items-center justify-content-center">
                    <h3 class="font-family-montserrat font-size-22 font-color-color-1 text-uppercase">Income</h3>
                    <h1 class="font-family-montserrat font-size-42 font-color-color-1 text-uppercase font-weight-bold">Projection</h1>
                    <div class="separator mb-60"></div>
                </div>
            </div>

            <!-- Card -->
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <!-- Header -->
                        <div class="card-header">
                            <div class="row align-items-center justify-content-between">

                                <!-- Community -->
                                <div class="col-12 col-md-6 col-lg divisor">
                                    <div class="community-container d-flex align-items-center justify-content-start justify-content-md-center">
                                        <div class="d-inline-flex align-items-center icon-map-marked-gradient mr-10"></div>
                                        <span class="d-inline-flex align-items-center community-text text-container font-family-montserrat font-color-color-1 font-size-16 font-weight-bold text-uppercase">
                                            Champions Gate
                                        </span>
                                    </div>
                                </div>

                                <!-- Property Type -->
                                <div class="col-12 col-md-6 col-lg divisor">
                                    <div class="property-container d-flex align-items-center justify-content-start justify-content-md-center">
                                        <div class="d-inline-flex align-items-center icon-home-gradient mr-5"></div>
                                        <span class="d-inline-flex align-items-center property-text text-container font-family-montserrat font-color-color-1 font-size-16 font-weight-bold text-uppercase">
                                            Single Family
                                        </span>
                                    </div>
                                </div>

                                <!-- Beds -->
                                <div class=" col-12 col-md-6 col-lg divisor">
                                    <div class="beds-container d-flex align-items-center justify-content-start justify-content-md-center">
                                        <div class="d-inline-flex align-items-center icon-bed-gradient mr-5"></div>
                                        <span class="d-inline-flex align-items-center beds-text text-container font-family-montserrat font-color-color-1 font-size-16 font-weight-bold text-uppercase">
                                            8-9
                                        </span>
                                    </div>
                                </div>

                                <!-- Property Price -->
                                <div class="col-12 col-md-6 col-lg divisor">
                                    <div class="price-container d-flex align-items-center justify-content-start justify-content-md-center">
                                        <div class="d-inline-flex align-items-center icon-financing-gradient mr-5"></div>
                                        <span class="d-inline-flex align-items-center price-text text-container font-family-montserrat font-color-color-1 font-size-16 font-weight-bold text-uppercase">
                                            $570,000
                                        </span>
                                    </div>
                                </div>

                                <!-- Average Nightly -->
                                <div class="col-12 col-md-6 col-lg">
                                    <div class="income-container d-flex align-items-center justify-content-start justify-content-md-center">
                                        <div class="d-inline-flex align-items-center icon-income-project-gradient mr-5"></div>
                                        <span class="d-inline-flex align-items-center income-text text-container font-family-montserrat font-color-color-1 font-size-16 font-weight-bold text-uppercase">
                                            $570,000
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-5 pl-15 pr-15">
                                    <div class="img-container w-100 mb-20">
                                        <img src="./img/img-615.png" class="w-100" alt="Flor Plan">
                                        <div class="watermark d-flex align-items-center justify-content-center">
                                            Main picture of a standard house
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-lg-7 pl-15 pr-15">
                                    <div class="slide-container mb-20">
                                        <div class="floor-plan-slider">
                                            <div><img class="img-slider" src="./img/img-123.png" alt="Plan"></div>
                                            <div><img class="img-slider" src="./img/img-123.png" alt="Plan"></div>
                                            <div><img class="img-slider" src="./img/img-123.png" alt="Plan"></div>
                                        </div>
                                    </div>
                                    <div class="slide-controls">
                                        <!-- Btn next image -->
                                        <button class="btn-prev">
                                            <img src="./img/slide-prev-button.png" alt="Next Button">
                                        </button>
                                        <!-- Flor image name -->
                                        <span class="font-family-montserrat font-size-16 font-color-color-1
                                                     font-weight-bold text-uppercase">First Floor</span>
                                        <!-- Btn prev image -->
                                        <button class="btn-next">
                                            <img src="./img/slide-next-button.png" alt="Prev Button">
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- Footer -->
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-12">

                                    <!-- Flor Nav -->
                                    <nav class="floor-nav">
                                        <ul class="floor-nav-items justify-content-lg-around font-family-montserrat font-size-16 font-color-color-1 font-weight-bold text-uppercase">
                                            <li class="floor-nav-item active">1-2 beds</li>
                                            <li class="floor-nav-item">2-3 beds</li>
                                            <li class="floor-nav-item">3-4 beds</li>
                                            <li class="floor-nav-item">4-5 beds</li>
                                            <li class="floor-nav-item">5-6 beds</li>
                                            <li class="floor-nav-item">6-7 beds</li>
                                            <li class="floor-nav-item">7-8 beds</li>
                                            <li class="floor-nav-item">8-9 beds</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Overview -->
    <section id="overview">
        <div class="container">

            <!-- Section Title -->
            <div class="row">
                <div class="col d-flex flex-column align-items-center justify-content-center">
                    <h1 class="font-family-montserrat font-size-42 font-color-color-1 text-uppercase font-weight-bold">Overview</h1>
                    <div class="separator mb-60"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="font-family-montserrat font-size-27 font-color-color-1
                                       font-weight-bold text-uppercase">Celebration Florida Homes for Sale:</h3>
                            <h3 class="font-family-montserrat font-size-27 font-color-gradient
                                       font-weight-bold text-uppercase">Celebration Real Estate</h3>
                        </div>
                        <div class="card-body content-container">
                                <div class="row">
                                    <div class="col-12 col-md-6 pl-15 pr-15">
                                        <p>If you are looking for Celebration Florida homes for sale you need to know it was
                                            built by Disney, Celebration is a master-planned community in Kissimmee, Florida.</p>
                                        <p>With no fewer than 15 distinct subdivisions, it's amenable to an array of lifestyles.
                                            Love golf? The golf course views at North Village might be on par with your tastes. Have kids?</p>
                                        <p>West Village puts you within walking distance of Celebration School, one of the
                                            top-rated k-8's in the state! Celebration, Florida also boasts a wide range of
                                            homes for sale. From single-family houses, to condos, to townhomes, in Celebration,
                                            FL, you'll find homes for sale for every taste. And with prices ranging from the
                                            low 200's up to 7 million dollars, homes for every budget! Of course, with so much variation, narrowing it down is a challenge. But at Orlando Homes For Sale, our experience and knowledge let us help you hone in on the perfect Celebration, Florida houses, townhomes, or condos for your needs!</p>
                                    </div>
                                    <div class="col-12 col-md-6 pl-15 pr-15">
                                        <p> in Celebration, FL, you'll find homes for sale for every taste. And with prices ranging
                                            from the low 200's up to 7 million dollars, homes for every budget! Of course,
                                            with so much variation, narrowing it down is a challenge. But at Orlando Homes For Sale,
                                            our experience and knowledge let us help you hone in on the perfect Celebration,
                                            Florida houses, townhomes, or condos for your needs!</p>
                                        <p>See below the best Celebration Florida real estate for sale, when your dream is by the best deal</p>
                                        <p>in Celebration, FL, you'll find homes for sale for every taste. And with prices
                                            ranging from the low 200's up to 7 million dollars, homes for every budget!
                                            Of course, with so much variation, narrowing it down is a challenge. But at Orlando
                                            Homes For Sale, our experience and knowledge let us help you hone in on the perfect
                                            Celebration, Florida houses, townhomes, or condos for your needs!</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <img src="./img/img-14.png" class="w-100 mt-40 mb-40" alt="">
                                    </div>
                                    <div class="col-12 col-md-6 pl-15 pr-15">
                                        <p>If you are looking for Celebration Florida homes for sale you need to know it was
                                            built by Disney, Celebration is a master-planned community in Kissimmee, Florida.</p>
                                        <p>With no fewer than 15 distinct subdivisions, it's amenable to an array of lifestyles.
                                            Love golf? The golf course views at North Village might be on par with your tastes. Have kids?</p>
                                        <p>West Village puts you within walking distance of Celebration School, one of the
                                            top-rated k-8's in the state! Celebration, Florida also boasts a wide range of
                                            homes for sale. From single-family houses, to condos, to townhomes, in Celebration,
                                            FL, you'll find homes for sale for every taste. And with prices ranging from the
                                            low 200's up to 7 million dollars, homes for every budget! Of course, with so much variation, narrowing it down is a challenge. But at Orlando Homes For Sale, our experience and knowledge let us help you hone in on the perfect Celebration, Florida houses, townhomes, or condos for your needs!</p>
                                    </div>
                                    <div class="col-12 col-md-6 pl-15 pr-15">
                                        <p> in Celebration, FL, you'll find homes for sale for every taste. And with prices ranging
                                            from the low 200's up to 7 million dollars, homes for every budget! Of course,
                                            with so much variation, narrowing it down is a challenge. But at Orlando Homes For Sale,
                                            our experience and knowledge let us help you hone in on the perfect Celebration,
                                            Florida houses, townhomes, or condos for your needs!</p>
                                        <p>See below the best Celebration Florida real estate for sale, when your dream is by the best deal</p>
                                        <p>in Celebration, FL, you'll find homes for sale for every taste. And with prices
                                            ranging from the low 200's up to 7 million dollars, homes for every budget!
                                            Of course, with so much variation, narrowing it down is a challenge. But at Orlando
                                            Homes For Sale, our experience and knowledge let us help you hone in on the perfect
                                            Celebration, Florida houses, townhomes, or condos for your needs!</p>
                                    </div>
                                </div>
                                <div class="separator-full"></div>
                                <div class="row">
                                    <div class="col-12 col-lg-6 pl-15 pr-15">
                                        <img src="./img/img-101.png" class="w-100" alt="">
                                    </div>
                                    <div class="col-12 col-lg-6 pl-15 pr-15">
                                        <h3>Celebration Florida<br>Homes for Sale:</h3>
                                        <p> in Celebration, FL, you'll find homes for sale for every taste. And with prices ranging
                                            from the low 200's up to 7 million dollars, homes for every budget! Of course,
                                            with so much variation, narrowing it down is a challenge. But at Orlando Homes For Sale,
                                            our experience and knowledge let us help you hone in on the perfect Celebration,
                                            Florida houses, townhomes, or condos for your needs!</p>
                                        <p>in Celebration, FL, you'll find homes for sale for every taste. And with prices
                                            ranging from the low 200's up to 7 million dollars, homes for every budget!
                                            Of course, with so much variation, narrowing it down is a challenge.</p>
                                    </div>
                                    <div class="col-12 pl-15 pr-15">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non nulla
                                            at erat faucibus rutrum. Etiam ac rhoncus risus, quis eleifend eros. Aenean tristique,
                                            enim vel lobortis aliquam, ligula nibh eleifend elit, vel luctus felis neque a nunc.
                                            Proin eget neque eget tortor pulvinar tempus non sit amet dui. Aenean non felis nec
                                            justo eleifend aliquet ut sit amet nisi. Interdum et malesuada fames ac ante ipsum
                                            primis in faucibus. Proin pellentesque lectus posuere justo rutrum mollis. Mauris
                                            porttitor erat vitae massa feugiat, eu tincidunt sapien finibus. Pellentesque ante lacus,
                                            euismod nec gravida eu, placerat ut felis. Donec et sagittis ex. Nulla facilisi.
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam posuere dui eu sem
                                            varius tristique.</p>
                                    </div>
                                    <div class="col-12 pl-15 pr-15">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non nulla
                                            at erat faucibus rutrum. Etiam ac rhoncus risus, quis eleifend eros. Aenean tristique,
                                            enim vel lobortis aliquam, ligula nibh eleifend elit, vel luctus felis neque a nunc.
                                            Proin eget neque eget tortor pulvinar tempus non sit amet dui. Aenean non felis nec
                                            justo eleifend aliquet ut sit amet nisi. Interdum et malesuada fames ac ante ipsum
                                            primis in faucibus. Proin pellentesque lectus posuere justo rutrum mollis. Mauris
                                            porttitor erat vitae massa feugiat, eu tincidunt sapien finibus. Pellentesque ante lacus,
                                            euismod nec gravida eu, placerat ut felis. Donec et sagittis ex. Nulla facilisi.
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam posuere dui eu sem
                                            varius tristique.</p>
                                    </div>
                                </div>
                                <div class="separator-full"></div>
                                <div class="row">
                                    <div class="col-12 col-lg-6 pl-15 pr-15">
                                        <h3>Celebration Florida<br>Homes for Sale:</h3>
                                        <p> in Celebration, FL, you'll find homes for sale for every taste. And with prices ranging
                                            from the low 200's up to 7 million dollars, homes for every budget! Of course,
                                            with so much variation, narrowing it down is a challenge. But at Orlando Homes For Sale,
                                            our experience and knowledge let us help you hone in on the perfect Celebration,
                                            Florida houses, townhomes, or condos for your needs!</p>
                                        <p>in Celebration, FL, you'll find homes for sale for every taste. And with prices
                                            ranging from the low 200's up to 7 million dollars, homes for every budget!
                                            Of course, with so much variation, narrowing it down is a challenge.</p>
                                    </div>
                                    <div class="col-12 col-lg-6 pl-15 pr-15">
                                        <img src="./img/img-101.png" class="w-100" alt="">
                                    </div>
                                    <div class="col-12 pl-15 pr-15">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non nulla
                                            at erat faucibus rutrum. Etiam ac rhoncus risus, quis eleifend eros. Aenean tristique,
                                            enim vel lobortis aliquam, ligula nibh eleifend elit, vel luctus felis neque a nunc.
                                            Proin eget neque eget tortor pulvinar tempus non sit amet dui. Aenean non felis nec
                                            justo eleifend aliquet ut sit amet nisi. Interdum et malesuada fames ac ante ipsum
                                            primis in faucibus. Proin pellentesque lectus posuere justo rutrum mollis. Mauris
                                            porttitor erat vitae massa feugiat, eu tincidunt sapien finibus. Pellentesque ante lacus,
                                            euismod nec gravida eu, placerat ut felis. Donec et sagittis ex. Nulla facilisi.
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam posuere dui eu sem
                                            varius tristique.</p>
                                    </div>
                                    <div class="col-12 pl-15 pr-15">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non nulla
                                            at erat faucibus rutrum. Etiam ac rhoncus risus, quis eleifend eros. Aenean tristique,
                                            enim vel lobortis aliquam, ligula nibh eleifend elit, vel luctus felis neque a nunc.
                                            Proin eget neque eget tortor pulvinar tempus non sit amet dui. Aenean non felis nec
                                            justo eleifend aliquet ut sit amet nisi. Interdum et malesuada fames ac ante ipsum
                                            primis in faucibus. Proin pellentesque lectus posuere justo rutrum mollis. Mauris
                                            porttitor erat vitae massa feugiat, eu tincidunt sapien finibus. Pellentesque ante lacus,
                                            euismod nec gravida eu, placerat ut felis. Donec et sagittis ex. Nulla facilisi.
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam posuere dui eu sem
                                            varius tristique.</p>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Newsletter  -->
    <section id="newsletter">
        <div class="container">

            <!-- Title -->
            <div class="row">
                <div class="col d-flex flex-column align-items-center justify-content-center">
                    <h3 class="font-family-montserrat font-size-22 font-color-white text-uppercase">SUBSCRIBE TO OUT</h3>
                    <h1 class="font-family-montserrat font-size-42 font-color-white text-uppercase font-weight-bold">NEWSLETTER</h1>
                    <div class="separator mb-40"></div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <form action="" class="d-flex flex-column justify-content-center align-items-center
                                       d-lg-inline-flex flex-lg-row align-items-lg-center justify-content-lg-center">
                        <div class="col-12 col-lg-4 offset-lg-2 pr-15 pl-15 text-container">
                            Keep up to date with the lastest News, trens, communities, special delas and more!
                        </div>
                        <div class="col-12 col-lg-4 pr-15 pl-15">
                            <input type="email" class="form-control mt-15 mb-15" placeholder="Your email">
                        </div>
                        <div class="col-12 col-lg-2 d-flex justify-content-center pr-15 pl-15">
                            <button class="btn btn-gradient icon-arrow-right">
                                <span class="mr-5">subscribe</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>
    
    <footer>
        <div class="container">
            <!-- logo -->
            <div class="row pt-100 pb-60" style="border-bottom: 1px solid #E4E4E4">
                <div class="col-12">
                    <ul class="d-flex flex-column flex-md-row align-items-center justify-content-center">
                        <li><img src="./img/logo-orlando-color.svg" class="logo" alt="Logo Orlando"></li>
                        <li><div class="separator-vertical d-none d-md-block ml-40 mr-40"></div></li>
                        <li><img src="./img/logo-authentic-color.png" class="logo" alt="Logo Authentic"></li>
                    </ul>
                </div>
            </div>

            <!-- Navs -->
            <div class="row pt-60 pb-60">
                <div class="col d-none d-lg-block">
                    <nav>
                        <div class="title">Find a Home</div>
                        <ul>
                            <li><a href="#">Residential Homes</a></li>
                            <li><a href="#">Vacation Rentals</a></li>
                            <li><a href="#">Luxury Properties</a></li>
                            <li><a href="#">New Communities</a></li>
                            <li><a href="#">Condos</a></li>
                            <li><a href="#">Search by Area</a></li>
                            <li><a href="#">Search the MLS</a></li>
                            <li><a href="#">Lease</a></li>
                            <li><a href="#">Book a Reservation</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col d-none d-lg-block">
                    <nav>
                        <div class="title">Areas</div>
                        <ul>
                            <li><a href="#">Celebration</a></li>
                            <li><a href="#">Champions gate</a></li>
                            <li><a href="#">Clermont</a></li>
                            <li><a href="#">Davenport</a></li>
                            <li><a href="#">Disney World</a></li>
                            <li><a href="#">Dr. Phillips</a></li>
                            <li><a href="#">Hunter’s Creek</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col d-none d-lg-block">
                    <nav>
                        <ul style="margin-top: 45px">
                            <li><a href="#">Kissimmee</a></li>
                            <li><a href="#">Lake Nona</a></li>
                            <li><a href="#">Montverde</a></li>
                            <li><a href="#">Windermere</a></li>
                            <li><a href="#">Winter Garden</a></li>
                            <li><a href="#">Winter Park</a></li>
                            <li><a href="#">Reunion</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col d-none d-lg-block">
                    <nav>
                        <div class="title">RESOURCES</div>
                        <ul>
                            <li><a href="#">For Buyers</a></li>
                            <li><a href="#">For Sellers</a></li>
                            <li><a href="#">For Investors</a></li>
                            <li><a href="#">Foreign Clients</a></li>
                            <li><a href="#">Get Financing</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col d-none d-lg-block">
                    <nav>
                        <div class="title">RESOURCES</div>
                        <ul>
                            <li><a href="#">Who we are</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Videos</a></li>
                            <li><a href="#">Featured Medias</a></li>
                            <li><a href="#">Testimonials</a></li>
                            <li><a href="#">Be our Partner</a></li>
                            <li><a href="#">Workshop</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col col-lg-3">
                    <address>
                        <ul>
                            <li class="d-flex align-items-center mb-25">
                                <div class="icon-place-gradient mr-15"></div>
                                <div class="address-container d-flex flex-column justify-content-center align-items-start">
                                    <span class="font-weight-bold">Address:</span>
                                    <span>Address: 380 Sand Lake Road, Suite 500, Orlando, FL 32819 - US</span>
                                </div>
                            </li>
                            <li class="d-flex align-items-center mb-25">
                                <div class="icon-phone-gradient mr-15"></div>
                                <div class="address-container d-flex flex-column justify-content-center align-items-start">
                                    <span class="font-weight-bold">Phone:</span>
                                    <span>+1 (407) 440.1010</span>
                                </div>
                            </li>
                            <li>
                                <button class="btn btn-color-1">CONTACT US</button>
                            </li>
                        </ul>
                    </address>
                </div>
            </div>
        </div>

        <div class="copyrights d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col d-flex flex-column flex-md-row justify-content-center align-items-center justify-content-md-between">

                        <!-- Text Container -->
                        <div class="text-container">
                            <span class="font-weight-bold">© 2021 Orlando Homes For Sale.</span> All rights reserved –
                            <a href="#">Privacy Policy</a> –
                            <a href="#">Sitemap</a> – Orlando Homes For Sale
                        </div>

                        <!-- Social Container -->
                        <div class="social-container">
                            <ul class="d-flex align-items-center">
                                <li>
                                    <a href="https://www.facebook.com/casasavendaorlando/" target="_blank">
                                        <img class="icon mr-15" src="./img/icon-facebook-square.svg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/authenticorlando/" target="_blank">
                                        <img class="icon mr-15" src="./img/icon-instagram.svg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/c/authenticrealestate" target="_blank">
                                        <img class="icon mr-15" src="./img/icon-youtube-square.svg" alt="">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="js/jquery.js"></script>
    <script src="js/tiny-slider.js"></script>
    <script src="js/readmore.min.js"></script>
    <script src="js/index.js"></script>
</body>
</html>