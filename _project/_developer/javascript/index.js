$(document).ready(function (){

    /**** Set Vars ****/
    var mainNav = $('.main-nav')
    var actionButton = $('.main-nav-action').find('img')
    var slider = tns({
        container: '.floor-plan-slider',
        items: 1,
        mouseDrag: true,
        controls: false
    });

    /*** Nav Controls ***/
    $('.main-nav-action').on('click', function (){
        if (mainNav.hasClass('main-nav-hide')) {
            mainNav.removeClass('main-nav-hide')
            mainNav.addClass('main-nav-show')
            actionButton.css('filter', 'brightness(10)')
        } else {
            mainNav.removeClass('main-nav-show')
            mainNav.addClass('main-nav-hide')
            actionButton.css('filter', 'brightness(1)')
        }
    })

    $('.secondary-nav-item').on('click', function () {
        $('.secondary-nav-item').removeClass('active');
        $(this).addClass('active');
    })

    $('.table-nav-item').on('click', function () {
        $('.table-nav-item').removeClass('active');
        $(this).addClass('active');
    })

    $('.floor-nav-item').on('click', function () {
        $('.floor-nav-item').removeClass('active');
        $(this).addClass('active');
    })

    /*** Slide Actions ***/
    $('.btn-next').on('click', function () {
        slider.goTo('next');
    })

    $('.btn-prev').on('click', function () {
        slider.goTo('prev');
    })

    /*** Read More ***/
    $('.content-container').readmore({
        speed: 75,
        moreLink: '<div class="card-footer d-flex align-items-center">' +
                    '<button class="btn btn-lg btn-gradient btn-read-more icon-arrow-right" data-readmore-toggle="rmjs-1" aria-controls="rmjs-1">' +
                        '<span class="mr-15">Read More</span>' +
                    '</button>' +
                   '</div>',
        lessLink: '<div class="card-footer d-flex align-items-center">' +
                    '<button class="btn btn-lg btn-gradient btn-read-more icon-arrow-right" data-readmore-toggle="rmjs-1" aria-controls="rmjs-1">' +
                        '<span class="mr-15">Read Less</span>' +
                    '</button>' +
                  '</div>'
    });
})