module.exports = function(grunt) {
    grunt.initConfig({
        copy: {
            public: {
                expand: true,
                cwd: '_project/_developer/javascript',
                src: ['*.js'],
                dest: '_project/js'
            },
            tinyjs: {
                expand: true,
                cwd: 'node_modules/tiny-slider/dist/min',
                src: 'tiny-slider.js',
                dest: '_project/js'
            },
            tinycss: {
                expand: true,
                cwd: 'node_modules/tiny-slider/dist',
                src: 'tiny-slider.css',
                dest: '_project/css'
            },
            readmorejs: {
                expand: true,
                cwd: 'node_modules/readmore-js',
                src: 'readmore.min.js',
                dest: '_project/js'
            },
        },
        useminPrepare: {
            html: ['_project/css/*.css', '_project/js/*.js']
        },
        usemin: {
            html: ['_project/css/*.css', '_project/js/*.js']
        },
        sass: {
            compilar: {
                expand: true,
                cwd: '_project/_developer/sass',
                src: '**/index.scss',
                dest: '_project/css',
                ext: '.css'
            }
        },
        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: [{
                    expand: true,
                    cwd: '_project/css',
                    src: '**/index.css',
                    dest: '_project/css',
                    ext: '.css'
                }]
            }
        },
        uglify: {
            target: {
                files: {
                    '_project/js/jquery.js': '_project/js/jquery.js',
                    '_project/js/index.js': '_project/js/index.js',
                }
            }
        },
        watch:{
            options: {
                event: ['added', 'changed']
            },
            development: {
                files: ['_project/_developer/sass/**/*.scss', '_project/_developer/javascript/*.js'],
                tasks: 'development'
            }
        }
    });

    //Development
    grunt.registerTask('development', ['copy', 'sass']);
    
    //Distribution
    grunt.registerTask('dist', ['copy', 'sass', 'cssmin', 'uglify']);

    //Load NPM
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
};